<?php
/**
 * @file
 * wise_theme.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_theme_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'groene_bol_groenestroom';
  $context->description = '';
  $context->tag = 'Groene bol';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'initiatief' => 'initiatief',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'node_taxonomy' => array(
      'values' => array(
        37 => 37,
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'groene-stroom/*' => 'groene-stroom/*',
        '~groene-stroom/leverancier/*' => '~groene-stroom/leverancier/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'finder-content_finder' => array(
          'module' => 'finder',
          'delta' => 'content_finder',
          'region' => 'sidebar_first',
          'weight' => '-43',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Groene bol');
  $export['groene_bol_groenestroom'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'groene_bol_kernenergie';
  $context->description = '';
  $context->tag = 'Groene bol';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'kernenergie/*' => 'kernenergie/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4050' => array(
          'module' => 'nodeblock',
          'delta' => '4050',
          'region' => 'sidebar_first',
          'weight' => '-41',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Groene bol');
  $export['groene_bol_kernenergie'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'groene_bol_overig';
  $context->description = '';
  $context->tag = 'Groene bol';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'doe-mee' => 'doe-mee',
        'doe-mee/doe-mee' => 'doe-mee/doe-mee',
        'wat-we-doen' => 'wat-we-doen',
        'over-ons' => 'over-ons',
        'over-ons/*' => 'over-ons/*',
        'wat-we-doen/*' => 'wat-we-doen/*',
        'event/*' => 'event/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'finder-content_finder' => array(
          'module' => 'finder',
          'delta' => 'content_finder',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'nodeblock-4050' => array(
          'module' => 'nodeblock',
          'delta' => '4050',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Groene bol');
  $export['groene_bol_overig'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'homepage';
  $context->description = '';
  $context->tag = 'algemeen';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-actueel-block' => array(
          'module' => 'views',
          'delta' => 'actueel-block',
          'region' => 'featured_1',
          'weight' => '-10',
        ),
        'nodeblock-3497' => array(
          'module' => 'nodeblock',
          'delta' => '3497',
          'region' => 'featured_2',
          'weight' => '-10',
        ),
        'finder-content_finder' => array(
          'module' => 'finder',
          'delta' => 'content_finder',
          'region' => 'featured_3',
          'weight' => '-9',
        ),
        'nodeblock-3495' => array(
          'module' => 'nodeblock',
          'delta' => '3495',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'nodeblock-4063' => array(
          'module' => 'nodeblock',
          'delta' => '4063',
          'region' => 'content',
          'weight' => '-10',
        ),
        'nodeblock-30' => array(
          'module' => 'nodeblock',
          'delta' => '30',
          'region' => 'triptych_first',
          'weight' => '-10',
        ),
        'nodeblock-4066' => array(
          'module' => 'nodeblock',
          'delta' => '4066',
          'region' => 'triptych_middle',
          'weight' => '-10',
        ),
        'nodeblock-4065' => array(
          'module' => 'nodeblock',
          'delta' => '4065',
          'region' => 'triptych_last',
          'weight' => '-10',
        ),
        'nodeblock-4155' => array(
          'module' => 'nodeblock',
          'delta' => '4155',
          'region' => 'triptych2_first',
          'weight' => '-9',
        ),
        'nodeblock-4189' => array(
          'module' => 'nodeblock',
          'delta' => '4189',
          'region' => 'triptych2_middle',
          'weight' => '-9',
        ),
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'triptych2_last',
          'weight' => '-10',
        ),
        'nodeblock-4064' => array(
          'module' => 'nodeblock',
          'delta' => '4064',
          'region' => 'section_2',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('algemeen');
  $export['homepage'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'not_front';
  $context->description = '';
  $context->tag = 'algemeen';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-3496' => array(
          'module' => 'nodeblock',
          'delta' => '3496',
          'region' => 'featured_bg',
          'weight' => '43',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('algemeen');
  $export['not_front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'overige';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'over-ons/contact' => 'over-ons/contact',
        'contact' => 'contact',
        'webshop' => 'webshop',
        'over-ons/over-wise' => 'over-ons/over-wise',
        'over-wise' => 'over-wise',
        'user' => 'user',
        'user/*' => 'user/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4060' => array(
          'module' => 'nodeblock',
          'delta' => '4060',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['overige'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = '';
  $context->tag = 'algemeen';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'custom_search_blocks-1' => array(
          'module' => 'custom_search_blocks',
          'delta' => '1',
          'region' => 'search_block',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-10',
        ),
        'views-kernenergie_overzicht-block_1' => array(
          'module' => 'views',
          'delta' => 'kernenergie_overzicht-block_1',
          'region' => 'footer',
          'weight' => '-9',
        ),
        'views-groene_stroom-block_1' => array(
          'module' => 'views',
          'delta' => 'groene_stroom-block_1',
          'region' => 'footer',
          'weight' => '-8',
        ),
        'views-wat_we_doen-block_2' => array(
          'module' => 'views',
          'delta' => 'wat_we_doen-block_2',
          'region' => 'footer',
          'weight' => '-7',
        ),
        'views-partners-block' => array(
          'module' => 'views',
          'delta' => 'partners-block',
          'region' => 'partners',
          'weight' => '-10',
        ),
        'menu-menu-footer-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-footer-menu',
          'region' => 'below_footer',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('algemeen');
  $export['sitewide'] = $context;

  return $export;
}
