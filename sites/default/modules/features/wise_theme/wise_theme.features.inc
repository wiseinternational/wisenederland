<?php
/**
 * @file
 * wise_theme.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_theme_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function wise_theme_image_default_styles() {
  $styles = array();

  // Exported image style: homepage_fullwidth_header.
  $styles['homepage_fullwidth_header'] = array(
    'label' => 'Homepage_fullwidth_header',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1440,
          'height' => 668,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: nodeblock.
  $styles['nodeblock'] = array(
    'label' => 'Nodeblock',
    'effects' => array(
      10 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 262,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: rounded_200px.
  $styles['rounded_200px'] = array(
    'label' => 'Rounded 200px',
    'effects' => array(
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 200,
        ),
        'weight' => 1,
      ),
      7 => array(
        'name' => 'canvasactions_roundedcorners',
        'data' => array(
          'radius' => 100,
          'independent_corners_set' => array(
            'independent_corners' => 0,
            'radii' => array(
              'tl' => 0,
              'tr' => 0,
              'bl' => 0,
              'br' => 0,
            ),
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: subpage_block.
  $styles['subpage_block'] = array(
    'label' => 'Subpage block',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 160,
          'height' => 160,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: vervolgpagina_fullwidth_header.
  $styles['vervolgpagina_fullwidth_header'] = array(
    'label' => 'Vervolgpagina_fullwidth_header',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1440,
          'height' => 360,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function wise_theme_node_info() {
  $items = array(
    'home_background_image' => array(
      'name' => t('Home background image'),
      'base' => 'node_content',
      'description' => t('De afbeelding in de header van de homepage.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'nodeblock_algemeen' => array(
      'name' => t('Content block'),
      'base' => 'node_content',
      'description' => t('Content dat in een blok getoond moet worden.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'nodeblock_rechterkolom' => array(
      'name' => t('NodeBlock rechterkolom'),
      'base' => 'node_content',
      'description' => t('Content getoond in de rechter kolom.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'vervolgpagina_background_image' => array(
      'name' => t('Vervolgpagina background image'),
      'base' => 'node_content',
      'description' => t('De afbeelding in de header van alle pagina\'s behalve de voorpagina.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
