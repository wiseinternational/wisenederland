<?php
/**
 * @file
 * wise_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function wise_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: partners.
  $styles['partners'] = array(
    'label' => 'Partners',
    'effects' => array(
      8 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 100,
          'height' => '',
          'upscale' => 1,
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}
