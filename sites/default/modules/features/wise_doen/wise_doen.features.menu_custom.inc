<?php
/**
 * @file
 * wise_doen.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wise_doen_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-publicaties.
  $menus['menu-publicaties'] = array(
    'menu_name' => 'menu-publicaties',
    'title' => 'Publicaties',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Publicaties');

  return $menus;
}
