<?php
/**
 * @file
 * wise_doen.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_doen_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'doe_mee';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'doe-mee' => 'doe-mee',
        'doe-mee/*' => 'doe-mee/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4054' => array(
          'module' => 'nodeblock',
          'delta' => '4054',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'nodeblock-4154' => array(
          'module' => 'nodeblock',
          'delta' => '4154',
          'region' => 'highlighted',
          'weight' => '-48',
        ),
        'nodeblock-4117' => array(
          'module' => 'nodeblock',
          'delta' => '4117',
          'region' => 'highlighted',
          'weight' => '-40',
        ),
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '0',
        ),
        'nodeblock-4274' => array(
          'module' => 'nodeblock',
          'delta' => '4274',
          'region' => 'highlighted',
          'weight' => '-52',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['doe_mee'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'doneer';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'doneer' => 'doneer',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4059' => array(
          'module' => 'nodeblock',
          'delta' => '4059',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['doneer'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'stickers';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'kernenergie-nee-bedankt' => 'kernenergie-nee-bedankt',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4183' => array(
          'module' => 'nodeblock',
          'delta' => '4183',
          'region' => 'section_1',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['stickers'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'wat_we_doen';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'wat-we-doen' => 'wat-we-doen',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4057' => array(
          'module' => 'nodeblock',
          'delta' => '4057',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'nodeblock-4175' => array(
          'module' => 'nodeblock',
          'delta' => '4175',
          'region' => 'highlighted',
          'weight' => '-55',
        ),
        'nodeblock-4064' => array(
          'module' => 'nodeblock',
          'delta' => '4064',
          'region' => 'highlighted',
          'weight' => '-54',
        ),
        'nodeblock-4274' => array(
          'module' => 'nodeblock',
          'delta' => '4274',
          'region' => 'highlighted',
          'weight' => '-53',
        ),
        'block-3' => array(
          'module' => 'block',
          'delta' => '3',
          'region' => 'highlighted',
          'weight' => '-51',
        ),
        'nodeblock-4390' => array(
          'module' => 'nodeblock',
          'delta' => '4390',
          'region' => 'highlighted',
          'weight' => '-56',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['wat_we_doen'] = $context;

  return $export;
}
