<?php
/**
 * @file
 * wise_kernenergie.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wise_kernenergie_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-kernenergie.
  $menus['menu-kernenergie'] = array(
    'menu_name' => 'menu-kernenergie',
    'title' => 'Kernenergie algemeen',
    'description' => '',
  );
  // Exported menu: menu-kernenergie-anti-kernenergi.
  $menus['menu-kernenergie-anti-kernenergi'] = array(
    'menu_name' => 'menu-kernenergie-anti-kernenergi',
    'title' => 'Kernenergie anti kernenergie beweging',
    'description' => '',
  );
  // Exported menu: menu-kernenergie-kennis-nederlan.
  $menus['menu-kernenergie-kennis-nederlan'] = array(
    'menu_name' => 'menu-kernenergie-kennis-nederlan',
    'title' => 'Kernenergie Nederland',
    'description' => '',
  );
  // Exported menu: menu-uranium-in-afrika.
  $menus['menu-uranium-in-afrika'] = array(
    'menu_name' => 'menu-uranium-in-afrika',
    'title' => 'Kernenergie internationaal',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Kernenergie Nederland');
  t('Kernenergie algemeen');
  t('Kernenergie anti kernenergie beweging');
  t('Kernenergie internationaal');

  return $menus;
}
