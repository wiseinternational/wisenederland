<?php
/**
 * @file
 * wise_kernenergie.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_kernenergie_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'kern_detailpaginas';
  $context->description = '';
  $context->tag = 'Rechter kolom';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'kernenrgie/*' => 'kernenrgie/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu_block-4' => array(
          'module' => 'menu_block',
          'delta' => '4',
          'region' => 'sidebar_first',
          'weight' => '-37',
        ),
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'sidebar_first',
          'weight' => '-36',
        ),
        'menu_block-2' => array(
          'module' => 'menu_block',
          'delta' => '2',
          'region' => 'sidebar_first',
          'weight' => '-35',
        ),
        'menu_block-10' => array(
          'module' => 'menu_block',
          'delta' => '10',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Rechter kolom');
  $export['kern_detailpaginas'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'kernenergie';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'kernenergie/*' => 'kernenergie/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4055' => array(
          'module' => 'nodeblock',
          'delta' => '4055',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'menu_block-4' => array(
          'module' => 'menu_block',
          'delta' => '4',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'menu_block-2' => array(
          'module' => 'menu_block',
          'delta' => '2',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
        'menu_block-10' => array(
          'module' => 'menu_block',
          'delta' => '10',
          'region' => 'sidebar_first',
          'weight' => '-6',
        ),
        'block-9' => array(
          'module' => 'block',
          'delta' => '9',
          'region' => 'sidebar_first',
          'weight' => '-5',
        ),
        'twitter_profile_widget-0' => array(
          'module' => 'twitter_profile_widget',
          'delta' => '0',
          'region' => 'sidebar_first',
          'weight' => '-4',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['kernenergie'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'kernenergie_overzicht';
  $context->description = '';
  $context->tag = 'Subapagina';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'kernenergie' => 'kernenergie',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4055' => array(
          'module' => 'nodeblock',
          'delta' => '4055',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'nodeblock-4390' => array(
          'module' => 'nodeblock',
          'delta' => '4390',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'nodeblock-4274' => array(
          'module' => 'nodeblock',
          'delta' => '4274',
          'region' => 'highlighted',
          'weight' => '-9',
        ),
        'nodeblock-4064' => array(
          'module' => 'nodeblock',
          'delta' => '4064',
          'region' => 'highlighted',
          'weight' => '-6',
        ),
        'nodeblock-4117' => array(
          'module' => 'nodeblock',
          'delta' => '4117',
          'region' => 'highlighted',
          'weight' => '-5',
        ),
        'menu_block-4' => array(
          'module' => 'menu_block',
          'delta' => '4',
          'region' => 'content',
          'weight' => '-41',
        ),
        'menu_block-1' => array(
          'module' => 'menu_block',
          'delta' => '1',
          'region' => 'content',
          'weight' => '-40',
        ),
        'menu_block-2' => array(
          'module' => 'menu_block',
          'delta' => '2',
          'region' => 'content',
          'weight' => '-39',
        ),
        'menu_block-10' => array(
          'module' => 'menu_block',
          'delta' => '10',
          'region' => 'content',
          'weight' => '-38',
        ),
        'nodeblock-4050' => array(
          'module' => 'nodeblock',
          'delta' => '4050',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'block-9' => array(
          'module' => 'block',
          'delta' => '9',
          'region' => 'sidebar_first',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Subapagina');
  $export['kernenergie_overzicht'] = $context;

  return $export;
}
