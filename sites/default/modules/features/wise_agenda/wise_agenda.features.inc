<?php
/**
 * @file
 * wise_agenda.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_agenda_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_agenda_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_agenda_node_info() {
  $items = array(
    'agenda_item' => array(
      'name' => t('Agenda item'),
      'base' => 'node_content',
      'description' => t('Dit content type kun je gebruiken voor items waar een datum aan gekoppeld is.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_weight_features_default_settings().
 */
function wise_agenda_weight_features_default_settings() {
  $settings = array();

  $settings['agenda_item'] = array(
    'enabled' => 0,
    'range' => 20,
    'menu_weight' => 0,
    'default' => 0,
    'sync_translations' => 0,
  );

  return $settings;
}
