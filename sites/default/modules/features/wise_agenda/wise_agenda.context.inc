<?php
/**
 * @file
 * wise_agenda.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_agenda_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'agenda_actueel_block';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '~<front>' => '~<front>',
        '~actueel' => '~actueel',
        '~groenestroom' => '~groenestroom',
        '~groenestroom/*' => '~groenestroom/*',
        '~groene-stroom' => '~groene-stroom',
        '~groene-stroom/*' => '~groene-stroom/*',
        '~kernenergie' => '~kernenergie',
        '~kernenergie/*' => '~kernenergie/*',
        '~groene-energie-vergelijker' => '~groene-energie-vergelijker',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-agenda-block' => array(
          'module' => 'views',
          'delta' => 'agenda-block',
          'region' => 'section_1',
          'weight' => '-10',
        ),
        'views-actueel-block_1' => array(
          'module' => 'views',
          'delta' => 'actueel-block_1',
          'region' => 'section_1',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['agenda_actueel_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'agenda_actueel_block_groenestroom';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'groene-stroom' => 'groene-stroom',
        'groene-stroom/*' => 'groene-stroom/*',
        'groenestroom' => 'groenestroom',
        'groenestroom/*' => 'groenestroom/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-agenda-block' => array(
          'module' => 'views',
          'delta' => 'agenda-block',
          'region' => 'section_1',
          'weight' => '-10',
        ),
        'views-actueel-block_3' => array(
          'module' => 'views',
          'delta' => 'actueel-block_3',
          'region' => 'section_1',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['agenda_actueel_block_groenestroom'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'agenda_actueel_block_kernenergie';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'kernenergie' => 'kernenergie',
        'kernenergie/*' => 'kernenergie/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-agenda-block' => array(
          'module' => 'views',
          'delta' => 'agenda-block',
          'region' => 'section_1',
          'weight' => '-10',
        ),
        'views-actueel-block_4' => array(
          'module' => 'views',
          'delta' => 'actueel-block_4',
          'region' => 'section_1',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['agenda_actueel_block_kernenergie'] = $context;

  return $export;
}
