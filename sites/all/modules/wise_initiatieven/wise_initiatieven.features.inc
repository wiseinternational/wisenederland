<?php
/**
 * @file
 * wise_initiatieven.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_initiatieven_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "openlayers" && $api == "openlayers_maps") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_initiatieven_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_initiatieven_node_info() {
  $items = array(
    'initiatief' => array(
      'name' => t('Duurzame initiatieven'),
      'base' => 'node_content',
      'description' => t('Dit conent type verzamelt alle groene initiatieven op een kaartje op de GSJG site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_weight_features_default_settings().
 */
function wise_initiatieven_weight_features_default_settings() {
  $settings = array();

  $settings['initiatief'] = array(
    'enabled' => 0,
    'range' => 20,
    'menu_weight' => 0,
    'default' => 0,
    'sync_translations' => 0,
  );

  return $settings;
}
