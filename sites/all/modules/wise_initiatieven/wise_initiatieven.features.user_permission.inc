<?php
/**
 * @file
 * wise_initiatieven.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wise_initiatieven_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create initiatief content'.
  $permissions['create initiatief content'] = array(
    'name' => 'create initiatief content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any initiatief content'.
  $permissions['delete any initiatief content'] = array(
    'name' => 'delete any initiatief content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own initiatief content'.
  $permissions['delete own initiatief content'] = array(
    'name' => 'delete own initiatief content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any initiatief content'.
  $permissions['edit any initiatief content'] = array(
    'name' => 'edit any initiatief content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own initiatief content'.
  $permissions['edit own initiatief content'] = array(
    'name' => 'edit own initiatief content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
