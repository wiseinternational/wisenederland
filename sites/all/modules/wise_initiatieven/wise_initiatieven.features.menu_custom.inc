<?php
/**
 * @file
 * wise_initiatieven.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wise_initiatieven_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-groene-stroom-algemeen.
  $menus['menu-groene-stroom-algemeen'] = array(
    'menu_name' => 'menu-groene-stroom-algemeen',
    'title' => 'Groene stroom algemeen',
    'description' => '',
  );
  // Exported menu: menu-groene-stroom-garanties-van.
  $menus['menu-groene-stroom-garanties-van'] = array(
    'menu_name' => 'menu-groene-stroom-garanties-van',
    'title' => 'Groene stroom garanties van oorsprong',
    'description' => '',
  );
  // Exported menu: menu-groene-stroom-ja-graag-.
  $menus['menu-groene-stroom-ja-graag-'] = array(
    'menu_name' => 'menu-groene-stroom-ja-graag-',
    'title' => 'Groene stroom? Ja graag!',
    'description' => '',
  );
  // Exported menu: menu-groene-stroom-sjoemelstroom.
  $menus['menu-groene-stroom-sjoemelstroom'] = array(
    'menu_name' => 'menu-groene-stroom-sjoemelstroom',
    'title' => 'Groene stroom sjoemelstroom',
    'description' => '',
  );
  // Exported menu: menu-groene-stroom-wisselen-van-.
  $menus['menu-groene-stroom-wisselen-van-'] = array(
    'menu_name' => 'menu-groene-stroom-wisselen-van-',
    'title' => 'Groene stroom wisselen van stroom leverancier',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Groene stroom algemeen');
  t('Groene stroom garanties van oorsprong');
  t('Groene stroom sjoemelstroom');
  t('Groene stroom wisselen van stroom leverancier');
  t('Groene stroom? Ja graag!');

  return $menus;
}
