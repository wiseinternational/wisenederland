<?php
/**
 * @file
 * wise_initiatieven.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_initiatieven_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'groene_stroom';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'groene-stroom/*' => 'groene-stroom/*',
        '~groene-stroom/wise-groene-energie-vergelijker' => '~groene-stroom/wise-groene-energie-vergelijker',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4056' => array(
          'module' => 'nodeblock',
          'delta' => '4056',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['groene_stroom'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'groene_stroom_detailpaginas';
  $context->description = '';
  $context->tag = 'Rechter kolom';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'initiatief' => 'initiatief',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'node_taxonomy' => array(
      'values' => array(
        37 => 37,
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
    'path' => array(
      'values' => array(
        'groene-stroom/*' => 'groene-stroom/*',
        '~groene-stroom/leverancier/*' => '~groene-stroom/leverancier/*',
        '~groene-stroom/wise-groene-energie-vergelijker' => '~groene-stroom/wise-groene-energie-vergelijker',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu_block-5' => array(
          'module' => 'menu_block',
          'delta' => '5',
          'region' => 'sidebar_first',
          'weight' => '-37',
        ),
        'menu_block-6' => array(
          'module' => 'menu_block',
          'delta' => '6',
          'region' => 'sidebar_first',
          'weight' => '-36',
        ),
        'menu_block-7' => array(
          'module' => 'menu_block',
          'delta' => '7',
          'region' => 'sidebar_first',
          'weight' => '-35',
        ),
        'menu_block-8' => array(
          'module' => 'menu_block',
          'delta' => '8',
          'region' => 'sidebar_first',
          'weight' => '-34',
        ),
        'menu_block-11' => array(
          'module' => 'menu_block',
          'delta' => '11',
          'region' => 'sidebar_first',
          'weight' => '-31',
        ),
        'nodeblock-4117' => array(
          'module' => 'nodeblock',
          'delta' => '4117',
          'region' => 'sidebar_first',
          'weight' => '-6',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Rechter kolom');
  $export['groene_stroom_detailpaginas'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'groene_stroom_overzicht';
  $context->description = '';
  $context->tag = 'Subapagina';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'groene-stroom' => 'groene-stroom',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4056' => array(
          'module' => 'nodeblock',
          'delta' => '4056',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'nodeblock-4189' => array(
          'module' => 'nodeblock',
          'delta' => '4189',
          'region' => 'highlighted',
          'weight' => '-54',
        ),
        'finder-content_finder' => array(
          'module' => 'finder',
          'delta' => 'content_finder',
          'region' => 'highlighted',
          'weight' => '-53',
        ),
        'nodeblock-4153' => array(
          'module' => 'nodeblock',
          'delta' => '4153',
          'region' => 'highlighted',
          'weight' => '-52',
        ),
        'nodeblock-4117' => array(
          'module' => 'nodeblock',
          'delta' => '4117',
          'region' => 'highlighted',
          'weight' => '-51',
        ),
        'menu_block-5' => array(
          'module' => 'menu_block',
          'delta' => '5',
          'region' => 'content',
          'weight' => '-57',
        ),
        'menu_block-8' => array(
          'module' => 'menu_block',
          'delta' => '8',
          'region' => 'content',
          'weight' => '-56',
        ),
        'menu_block-7' => array(
          'module' => 'menu_block',
          'delta' => '7',
          'region' => 'content',
          'weight' => '-55',
        ),
        'menu_block-6' => array(
          'module' => 'menu_block',
          'delta' => '6',
          'region' => 'content',
          'weight' => '-54',
        ),
        'menu_block-11' => array(
          'module' => 'menu_block',
          'delta' => '11',
          'region' => 'content',
          'weight' => '-53',
        ),
        'nodeblock-4050' => array(
          'module' => 'nodeblock',
          'delta' => '4050',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'block-13' => array(
          'module' => 'block',
          'delta' => '13',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Subapagina');
  $export['groene_stroom_overzicht'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'vergelijker';
  $context->description = '';
  $context->tag = 'Page';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'groene-energie-vergelijker' => 'groene-energie-vergelijker',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4304' => array(
          'module' => 'nodeblock',
          'delta' => '4304',
          'region' => 'highlighted',
          'weight' => '-10',
        ),
        'nodeblock-4305' => array(
          'module' => 'nodeblock',
          'delta' => '4305',
          'region' => 'highlighted',
          'weight' => '-9',
        ),
        'block-17' => array(
          'module' => 'block',
          'delta' => '17',
          'region' => 'content',
          'weight' => '-10',
        ),
        'block-14' => array(
          'module' => 'block',
          'delta' => '14',
          'region' => 'section_2',
          'weight' => '-10',
        ),
        'nodeblock-4306' => array(
          'module' => 'nodeblock',
          'delta' => '4306',
          'region' => 'section_2',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Page');
  $export['vergelijker'] = $context;

  return $export;
}
