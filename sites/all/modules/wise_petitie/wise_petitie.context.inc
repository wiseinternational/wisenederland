<?php
/**
 * @file
 * wise_petitie.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_petitie_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'petitie_ondertekende';
  $context->description = '';
  $context->tag = 'webform';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'node/4255' => 'node/4255',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-petition_signatures-block_1' => array(
          'module' => 'views',
          'delta' => 'petition_signatures-block_1',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('webform');
  $export['petitie_ondertekende'] = $context;

  return $export;
}
