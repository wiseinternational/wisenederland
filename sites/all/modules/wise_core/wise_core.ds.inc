<?php
/**
 * @file
 * wise_core.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function wise_core_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'summary_and_body';
  $ds_view_mode->label = 'Summary and body';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['summary_and_body'] = $ds_view_mode;

  return $export;
}
