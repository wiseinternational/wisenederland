<?php
/**
 * @file
 * Displays a Zurb Foundation Section component.
 *
 * Available variables:
 * - $element array The element to render.
 * - $items array An array of section items to render.
 *
 * @ingroup themeable
 */
/** <?php print drupal_attributes($element['#attributes']) ?> */
?>

<?php if (!empty($items)) : ?>
  <dl class="accordion" data-accordion>
    <?php foreach ($items as $item) : ?>
      <dd<?php print drupal_attributes($item['container_attributes']) ?>>
        <a href="#<?php print $item['container_attributes']['id']?>"><?php print $item['title']; ?></a>
        <div id="<?php print $item['container_attributes']['id']?>" <?php print drupal_attributes($item['content_attributes']) ?>>
          <?php print $item['content']; ?>
        </div>
      </dd>
    <?php endforeach; ?>
  </dl>
<?php endif; ?>
