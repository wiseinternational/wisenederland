<?php

/**
 * Common mappings for the Drupal 6 node migrations.
 */
abstract class GroenestroomNodeMigration extends DrupalNode6Migration {
  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // @todo decide about this.
    //$this->addFieldMapping('field_legacy_nid', 'nid')
    //     ->description('We have a common field to save the D6 nid');
  }
}

class GroenestroomPageMigration extends GroenestroomNodeMigration {
  public function __construct(array $arguments) {
    // Add any other data we're pulling into the source row, before the parent
    // constructor.
    //$this->sourceFields['lead_graphic'] = 'Lead graphic string from D6';
    //$this->sourceFields['summary'] = 'Summary string from D6';

    parent::__construct($arguments);
    $this->addFieldMapping('field_document', 'upload')
      ->sourceMigration('GroenestroomFile');
    $this->addFieldMapping('field_document:file_class')
           ->defaultValue('MigrateFileFid');
  }
}

class GroenestroomArticleMigration extends GroenestroomNodeMigration {
  public function __construct(array $arguments) {
    // Add any other data we're pulling into the source row, before the parent
    // constructor.
    //$this->sourceFields['lead_graphic'] = 'Lead graphic string from D6';
    //$this->sourceFields['summary'] = 'Summary string from D6';

    parent::__construct($arguments);
    $this->addFieldMapping('field_image', 'upload')
      ->sourceMigration('GroenestroomFile');
    $this->addFieldMapping('field_image:file_class')
           ->defaultValue('MigrateFileFid');
    // Note that we map migrated terms by the vocabulary ID.
    $this->addFieldMapping('field_nieuws', '211')
         ->sourceMigration('NieuwsTags');
    $this->addFieldMapping('field_nieuws:source_type')
         ->defaultValue('tid');
  }
}

class GroenestroomStroometiketBedrijfMigration extends GroenestroomNodeMigration {
  public function __construct(array $arguments) {
    // Add any other data we're pulling into the source row, before the parent
    // constructor.
    //$this->sourceFields['lead_graphic'] = 'Lead graphic string from D6';
    //$this->sourceFields['summary'] = 'Summary string from D6';

    parent::__construct($arguments);
    $this->addFieldMapping('field_logo', 'upload')
      ->sourceMigration('GroenestroomFile');
    $this->addFieldMapping('field_logo:file_class')
           ->defaultValue('MigrateFileFid');
  }
}

class GroenestroomDuurzameInitiatievenMigration extends GroenestroomNodeMigration {
  public function __construct(array $arguments) {
    // Add any other data we're pulling into the source row, before the parent
    // constructor.
    $this->sourceFields['address_name'] = 'Address: name';
    $this->sourceFields['address_street'] = 'Address: street';
    $this->sourceFields['address_city'] = 'Address: city';
    $this->sourceFields['address_wkt'] = 'Address: WKT';

    parent::__construct($arguments);
    $this->addFieldMapping('field_link', 'field_link');
    $this->addFieldMapping('field_link:title', 'field_link:title');
    $this->addFieldMapping('field_address')
      ->defaultValue('NL');
    $this->addFieldMapping('field_address:name_line', 'address_name');
    $this->addFieldMapping('field_address:thoroughfare', 'address_street');
    $this->addFieldMapping('field_address:locality', 'address_city');
    $this->addFieldMapping('field_location', 'address_wkt');
  }

  public function prepareRow($current_row) {
    $lid = $current_row->field_locatie;
    $location = Database::getConnection('default', $this->sourceConnection)
                 ->select('location', 'l')
                 ->fields('l', array('name', 'street', 'city', 'latitude', 'longitude'))
                 ->condition('lid', $lid)
                 ->execute()
                 ->fetchObject();
    $current_row->address_name = $location->name;
    $current_row->address_street = $location->street;
    $current_row->address_city = $location->city;
    $current_row->address_wkt = 'POINT(' . $location->longitude . ' ' . $location->latitude . ')';
  }
}

class GroenestroomStroometiketJaarMigration extends GroenestroomNodeMigration {
  public function __construct(array $arguments) {
    // Add any other data we're pulling into the source row, before the parent
    // constructor.
    $this->sourceFields['jaar'] = 'Year';
    //$this->sourceFields['summary'] = 'Summary string from D6';

    parent::__construct($arguments);
    $this->addFieldMapping('field_bedrijf', 'field_bedrijf')
      ->sourceMigration('StroometiketBedrijf');
    $this->addFieldMapping('field_jaar', 'jaar');
    $this->addFieldMapping('field_milieukeur', 'field_milieukeur');
    $this->addFieldMapping('field_productie', 'field_productie');
    $this->addFieldMapping('field_kolen', 'field_kolen');
    $this->addFieldMapping('field_gas', 'field_gas');
    $this->addFieldMapping('field_kern', 'field_kern');
    $this->addFieldMapping('field_overig', 'field_overig');
    $this->addFieldMapping('field_wind', 'field_wind');
    $this->addFieldMapping('field_zon', 'field_zon');
    $this->addFieldMapping('field_water', 'field_water');
    $this->addFieldMapping('field_bio', 'field_bio');
    $this->addFieldMapping('field_co2', 'field_co2');
    $this->addFieldMapping('field_kernafval', 'field_kernafval');
    $this->addFieldMapping('field_document', 'upload')
      ->sourceMigration('GroenestroomFile');
    $this->addFieldMapping('field_document:file_class')
      ->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_document:display', 'upload:list')
      ->defaultValue('0');
  }

  public function prepareRow($current_row) {
    if (parent::prepareRow($current_row) === FALSE) {
      return FALSE;
    }
    $tid = $current_row->{'216'}[0];
    $term_row = Database::getConnection('default', $this->sourceConnection)
                 ->select('term_data', 't')
                 ->fields('t', array('name'))
                 ->condition('tid', $tid)
                 ->execute()
                 ->fetchObject();
    $current_row->jaar = array($term_row->name .'-01-01');
  }
}
