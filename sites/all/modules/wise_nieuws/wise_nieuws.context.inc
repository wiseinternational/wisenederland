<?php
/**
 * @file
 * wise_nieuws.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_nieuws_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'actueel';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'actueel' => 'actueel',
        'actueel/*' => 'actueel/*',
        'nieuws/*' => 'nieuws/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4058' => array(
          'module' => 'nodeblock',
          'delta' => '4058',
          'region' => 'featured_bg',
          'weight' => '-10',
        ),
        'views-agenda-block' => array(
          'module' => 'views',
          'delta' => 'agenda-block',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['actueel'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'actueel_nieuwsbrief_block';
  $context->description = '';
  $context->tag = 'Rechter kolom';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'actueel' => 'actueel',
        'actueel/*' => 'actueel/*',
        'over-ons' => 'over-ons',
        'over-ons/*' => 'over-ons/*',
        'contact' => 'contact',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-30' => array(
          'module' => 'nodeblock',
          'delta' => '30',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'block-9' => array(
          'module' => 'block',
          'delta' => '9',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'twitter_profile_widget-0' => array(
          'module' => 'twitter_profile_widget',
          'delta' => '0',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Rechter kolom');
  $export['actueel_nieuwsbrief_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'groene_bol_actueel';
  $context->description = '';
  $context->tag = 'Groene bol';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'actueel' => 'actueel',
        'actueel/*' => 'actueel/*',
        'nieuws/*' => 'nieuws/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4050' => array(
          'module' => 'nodeblock',
          'delta' => '4050',
          'region' => 'sidebar_first',
          'weight' => '-43',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Groene bol');
  $export['groene_bol_actueel'] = $context;

  return $export;
}
