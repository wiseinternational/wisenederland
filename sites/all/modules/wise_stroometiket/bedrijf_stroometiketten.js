(function($) {


  Drupal.behaviors.bedrijfStroometiketten = {
    attach: function (context, drupalSettings) {
      /* Globals */
      var data; /* Loaded with d3.json */
      var url = drupalSettings.wiseStroometiket.jsonUrl;
      var delay = d3.scale.ordinal()
        .range([0, 100, 200, 300, 1300, 1400, 1500, 1600]);
      var altColor = d3.scale.ordinal()
        .range(["#f57f21", "#f57f21", "#f57f21", "#f57f21", "#f57f21", "#f57f21", "#f57f21", "#f57f21"]);
      var color = d3.scale.ordinal()
        .range(["#2a7dbd", "#393939", "#828181", "#d63232", "#c9c8c8", "#76b8eb", "#ffde00", "#4aad01"]);
      var settings = [
        {name: 'Water', type: -0.5},
        {name: 'Gas', type: -1},
        {name: 'Kolen', type: -1},
        {name: 'Overig', type: -1},
        {name: 'Kern', type: -1},
        {name: 'Water', type: +0.5},
        {name: 'Wind', type: +1},
        {name: 'Bio', type: +1},
        {name: 'Zon', type: +1}
      ];
      var margin = {top: 75, right: 300, bottom: 50, left: 75},
        width = 960 - margin.left - margin.right,
        height = 1000 - margin.top - margin.bottom,
        circlesWidth = 120;
      var x; /* Scales calculated for svg */
      var y;
      var bedrijven = [];

      var radTransform = function (d, hover) {
        /* The size is relative to the svg, or?, the image doesn't seem to centre quite - argh */
        var scale = hover ?
            Math.sqrt(d["Radioactief afval per kWh"]) * 0.008 :
            d["Radioactief afval per kWh"] ? Math.log(Math.sqrt(d["Radioactief afval per kWh"])) * 0.008 : 0;
        var xt = hover ? width - x(d.negative[4].n1) - circlesWidth * scale : width - x(d.negative[4].n1) - circlesWidth * scale * 10,
          yt = hover ? y(d.Bedrijf + ' ' + d.Jaar) - 350 * scale : y(d.Bedrijf + ' ' + d.Jaar) + 0.1 / (scale ? scale : 1),
          rotate = hover ? 20 : 0;
        return 'translate(' + xt + ',' + yt + '), scale(' + scale + '), rotate(' + rotate + ')';
      };

      var initaliseGraph = function () {
      /** Settings **/
        delay.domain(d3.keys(data[0]).filter(function(key) { return key !== "Bedrijf" && key != 'Jaar' && key != 'Path'; }));
        altColor.domain(d3.keys(data[0]).filter(function(key) { return key !== "Bedrijf" && key != 'Jaar' && key != 'Path' && key !== 'CO₂ per kWh' && key !== 'Radioactief afval per kWh'; }));
        color.domain(d3.keys(data[0]).filter(function(key) { return key !== "Bedrijf" && key != 'Jaar' && key != 'Path' && key !== 'CO₂ per kWh' && key !== 'Radioactief afval per kWh'; }));
        /* Alter size related to number of rows */
        height = 40 * data.length;

        /** Retrieve company names **/
        var flags = [];
        for( var i=0; i<data.length; i++) {
          if( flags[data[i].Bedrijf] ) continue;
          flags[data[i].Bedrijf] = true;
          bedrijven.push(data[i].Bedrijf);
        }

        data.forEach(function(d) {
          var p0 = 0;
          var n0 = 0;
          d.negative = settings.filter(function(bar) { return bar.type < 0}).map(function(bar) {
            return {
              name: bar.name,
              n0: n0,
              n1: n0 += -(bar.type * d[bar.name]),
              total: d[bar.name],
            };
          });
          d.positive = settings.filter(function(bar) { return bar.type > 0 }).map(function(bar) {
            return {
              name: bar.name,
              p0: p0,
              p1: p0 += (bar.type * d[bar.name]),
              total: d[bar.name],
            }
          });
        });

        var legendSettings = settings;
        legendSettings.splice(0,1);
        /** Legend **/
        var legend = d3.select('#graph-legend').append('svg')
            .attr('width', 960)
            .attr('height', 30 )
          .selectAll("g")
            .data(legendSettings)
          .enter().append("g")
            .attr('transform', function(d, i) { return 'translate(' + i * 85 + ', 0)'; });

          legend.append("rect")
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", function(d) { return color(d.name); } )
            .attr("class", function(d) { return d.name; } );

          legend.append("text")
            .attr("x", 24)
            .attr("y", 15)
            .attr("dy", ".35em")
            .text(function(d) { return d.name; });

        bedrijven.forEach(function(d) {
          var bedrijfData = data.filter(function(j) { return j.Bedrijf == d });
        });

        /** Graph **/
        var svg = d3.select("#data-graph").append("svg")
          .attr("width", width + margin.left + circlesWidth + margin.right)
          .attr("height", height + margin.top + margin.bottom)
        .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        y = d3.scale.ordinal()
          .rangeRoundBands([height, 0], .1)
          .domain(data.map(function(d) { return d.Bedrijf + ' ' + d.Jaar; }));

        var yAxis = d3.svg.axis()
          .scale(y)
          .orient('right');

        x = d3.scale.linear()
          .domain([-100,100])
          .rangeRound([0, width]);
        var xAxis = d3.svg.axis()
          .scale(x)
          .orient("bottom")
          .tickFormat(function(d) {return d + '%';});
        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis);

        var row = svg.selectAll(".jaar")
            .data(data)
          .enter().append("g")
            .attr("class", "jaar")
            .attr("transform", function(d) { return "translate(0, " + y(d.Bedrijf + ' ' + d.Jaar) + ")"; });

        var rectPos = row.selectAll("rect.positive")
            .data(function(d) { return d.positive; }).enter().append('g');

        rectPos.append("rect")
            .attr("height", y.rangeBand())
            .attr("x", function(d) { return x(d.p0); })
            .style("fill", function(d) {return color(d.name); })
            .attr("class", function(d) {return d.name + ' positive has-tip'; } )
            .attr("width", 0)
          .transition()
            .duration(1000)
            .delay(function(d) {
              return delay(d.name);
            })
            .attr("width", function(d) {
              return x(d.p1) - x(d.p0);
            });

        var rectNeg = row.selectAll("rect.negative")
            .data(function(d) { return d.negative; }).enter().append('g');

        rectNeg.append("rect")
            .attr("height", y.rangeBand())
            .attr("x", function(d) { return x(-(d.n1)); })
            .style("fill", function(d) {return color(d.name); })
            .attr("class", function(d) {return d.name + ' negative has-tip'; } )
            .attr("width", function(d) {
              return x(d.n1) - x(d.n0);
            });
      /*
       * To do lhs need to draw growing SVG block from the right
            .attr("width", 0)
          .transition()
            .duration(1000)
            .ease('cubic-out-in')
            .delay(function(d) {
              return delay(d.name);
            })
            .attr("width", function(d) {
              return x(d.n0) - x(d.n1);
            });
      */

        svg.selectAll('rect')
          .append('title')
          .text(function (d) {
            return d.name + ' ' + d.total + '%';
          });

        svg.selectAll('.co2')
            .data(data)
          .enter().append('circle')
            .attr('class', 'co2 has-tip')
            .attr('cy', y.rangeBand() / 2)
            .style('fill', 'black')
            .attr('transform', function(d) {
              return 'translate(' + (width - 35 - Math.sqrt(d["CO₂ per kWh"]) - x(d.negative[4].n1)) + ', ' + y(d.Bedrijf + ' ' + d.Jaar)  + ')';
            })
          .transition()
            .duration(1000)
            .attr('r', function(d) {return Math.sqrt(d["CO₂ per kWh"]) / 2;});

        svg.selectAll('circle.co2')
          .append('title')
          .text(function (d) {
            return d["CO₂ per kWh"] + 'g CO₂ / kWh';
          });

        var rad = svg.selectAll('.rad')
            .data(data)
          .enter().append('svg:g');

        rad.filter(function(d) { return d["Radioactief afval per kWh"] != 0 }).append('path')
            .attr('class', 'rad has-tip')
        // Absolute
        //       .attr('d', "M 530,16 430,230 C 470,250 500,290 500,330 500,340 500,340 500,340 L 730,360 C 740,210 670,81 530,16 Z M 27,360 260,350 C 260,300 280,260 320,230 320,230 330,230 330,230 L 230,18 C 92,82 16,210 27,360 Z M 580,630 450,440 C 410,460 360,460 320,440 320,440 310,440 310,430 L 180,630 C 300,710 450,710 580,630 Z M 460,340 C 460,380 420,410 380,410 340,410 300,380 300,340 300,290 340,260 380,260 420,260 460,290 460,340 Z")
        // Relative
            .attr('d', "m 530,16 -100,214 c 40,20 70,60 70,100 0,10 0,10 0,10 l 230,20 c 10,-150 -60,-279 -200,-344 z m -503,344 233,-10 c 0,-50 20,-90 60,-120 0,0 10,0 10,0 l -100,-212 c -138,64 -214,192 -203,342 z m 553,270 -130,-190 c -40,20 -90,20 -130,0 0,0 -10,0 -10,-10 l -130,200 c 120,80 270,80 400,0 z m -120,-290 c 0,40 -40,70 -80,70 -40,0 -80,-30 -80,-70 0,-50 40,-80 80,-80 40,0 80,30 80,80 z")
            .style('stroke', "#FF0")
            .style('stroke-width', "45.1")
            .style('fill', '#000')
            .attr('transform', function(d) { return radTransform(d, 0); });

        svg.selectAll('path.rad')
          .append('title')
          .text(function (d) {
            return d.Jaar + ': ' + d["Radioactief afval per kWh"] + 'μg Rad.Afv / kWh';
          });

        d3.selectAll('path.rad')
          .on('mouseover', function(d, i) {
            d3.select(this)
              .transition()
                .duration(300)
                .ease('exp')
                .attr('transform', function(d) {return radTransform(d, 1); });
          })
          .on('mouseleave', function(d) {
            d3.select(this)
              .transition()
              .duration(300)
              .ease('back')
              .attr('transform', function(d) {return radTransform(d, 0); });
          });

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
            .attr('transform', 'translate(' + width / 2 + ', 0)');

        /* Move path, bar, under labels */
        svg.selectAll('.y.axis path')
          .remove();

        /* Background rectangle for text */
        var backgroundRect = svg.selectAll('.y.axis g')
          .insert('rect', 'text')
          .attr('width', function(d) { return this.parentNode.getBBox().width + 6; })
          .attr('height', function(d) { return this.parentNode.getBBox().height + 2; })
          .style('fill', '#cccccc')
          .style('fill-opacity', 0.6)
           .attr('transform', function(d) {
              return 'translate(-' + this.getBBox().width / 2 + ', -' + (-4 + y.rangeBand() / 2)  +')';
            });

       /* Make a link */
       svg.selectAll('.y.axis g.tick').append('a')
         .attr('xlink:href', function(d, e) {
           return data[e]['Path'];
         })
         .append(function() {
           var t = this.parentNode.getElementsByTagName('text');
           return t[0];
         });


        /* Move text to middle */
        svg.selectAll('.y.axis text')
            .attr('transform', function(d) {
              return 'translate(-' + (8 + this.getBBox().width / 2) + ', 0)';
            });


        d3.selectAll('rect')
          .on('mouseover', function(d, i) {
            d3.selectAll('.' + d.name).transition().style('fill', function(d) { return altColor(d.name); })
          })
          .on('mouseleave', function(d) {
            d3.selectAll('.' + d.name).transition().style('fill', function(d) { return color(d.name); })
          });

      /*
        Tooltips http://foundation.zurb.com/docs/components/tooltips.html
        $('svg title').parent().tipsy({
          html: true,
          gravity: 's',
        });
      */
      }

      d3.json(url, function(error, json) {
        if (error) return console.warn(error);
        data = json;
        initaliseGraph();
      });

    }
  };
})(jQuery);
