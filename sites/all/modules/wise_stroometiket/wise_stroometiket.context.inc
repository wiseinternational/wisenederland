<?php
/**
 * @file
 * wise_stroometiket.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wise_stroometiket_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'bedrijf_stroometiket';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'stroometiket_bedrijf' => 'stroometiket_bedrijf',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4158' => array(
          'module' => 'nodeblock',
          'delta' => '4158',
          'region' => 'content',
          'weight' => '35',
        ),
        'nodeblock-4187' => array(
          'module' => 'nodeblock',
          'delta' => '4187',
          'region' => 'sidebar_first',
          'weight' => '-15',
        ),
        'nodeblock-4117' => array(
          'module' => 'nodeblock',
          'delta' => '4117',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'nodeblock-4153' => array(
          'module' => 'nodeblock',
          'delta' => '4153',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['bedrijf_stroometiket'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'stroometiket_overzicht';
  $context->description = '';
  $context->tag = 'sectie';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'groene-stroom/stroometiketten' => 'groene-stroom/stroometiketten',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'nodeblock-4156' => array(
          'module' => 'nodeblock',
          'delta' => '4156',
          'region' => 'highlighted',
          'weight' => '-9',
        ),
        'nodeblock-4117' => array(
          'module' => 'nodeblock',
          'delta' => '4117',
          'region' => 'highlighted',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sectie');
  $export['stroometiket_overzicht'] = $context;

  return $export;
}
