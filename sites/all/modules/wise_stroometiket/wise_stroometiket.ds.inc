<?php
/**
 * @file
 * wise_stroometiket.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function wise_stroometiket_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|stroometiket_bedrijf|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'stroometiket_bedrijf';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'wise_stroometiket_etiket' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|stroometiket_bedrijf|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function wise_stroometiket_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|stroometiket_bedrijf|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'stroometiket_bedrijf';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'zf_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
        1 => 'field_logo',
        2 => 'wise_stroometiket_etiket',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
      'field_logo' => 'ds_content',
      'wise_stroometiket_etiket' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|stroometiket_bedrijf|default'] = $ds_layout;

  return $export;
}
