<?php
/**
 * @file
 * wise_stroometiket.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wise_stroometiket_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create stroometiket_bedrijf content'.
  $permissions['create stroometiket_bedrijf content'] = array(
    'name' => 'create stroometiket_bedrijf content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'create stroometiket_jaar content'.
  $permissions['create stroometiket_jaar content'] = array(
    'name' => 'create stroometiket_jaar content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any stroometiket_bedrijf content'.
  $permissions['delete any stroometiket_bedrijf content'] = array(
    'name' => 'delete any stroometiket_bedrijf content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete any stroometiket_jaar content'.
  $permissions['delete any stroometiket_jaar content'] = array(
    'name' => 'delete any stroometiket_jaar content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own stroometiket_bedrijf content'.
  $permissions['delete own stroometiket_bedrijf content'] = array(
    'name' => 'delete own stroometiket_bedrijf content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'delete own stroometiket_jaar content'.
  $permissions['delete own stroometiket_jaar content'] = array(
    'name' => 'delete own stroometiket_jaar content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any stroometiket_bedrijf content'.
  $permissions['edit any stroometiket_bedrijf content'] = array(
    'name' => 'edit any stroometiket_bedrijf content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit any stroometiket_jaar content'.
  $permissions['edit any stroometiket_jaar content'] = array(
    'name' => 'edit any stroometiket_jaar content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own stroometiket_bedrijf content'.
  $permissions['edit own stroometiket_bedrijf content'] = array(
    'name' => 'edit own stroometiket_bedrijf content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: 'edit own stroometiket_jaar content'.
  $permissions['edit own stroometiket_jaar content'] = array(
    'name' => 'edit own stroometiket_jaar content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
