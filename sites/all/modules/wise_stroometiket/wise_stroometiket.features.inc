<?php
/**
 * @file
 * wise_stroometiket.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wise_stroometiket_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wise_stroometiket_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wise_stroometiket_node_info() {
  $items = array(
    'stroometiket_bedrijf' => array(
      'name' => t('Stroometiket: Bedrijf'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'stroometiket_jaar' => array(
      'name' => t('Stroometiket: Jaar'),
      'base' => 'node_content',
      'description' => t('Stroometiket per jaar. Als je een nieuw jaar toe wilt voegen aan een bestaand bedrijf heb je wsl dit content-type nodig.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
